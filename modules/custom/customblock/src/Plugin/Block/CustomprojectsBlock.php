<?php

namespace Drupal\customblock\Plugin\Block;

use Drupal\user\Entity\User;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryFactory;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "customprojectsblock_block",
 *   admin_label = @Translation("Custom Block For Homepage Projects data"),
 * )
 */
class CustomprojectsBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
	 
    return [
      '#markup' => projects(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['customprojects_block_settings'] = $form_state->getValue('customprojects_block_settings');
  }
}